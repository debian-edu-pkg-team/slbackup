#!/bin/bash
#
# Script that stop slapd (and nscd), slapcat the ldap-database into a
# properly directory, and start (nscd and) slapd again.
#
# $Id: slapd_dump.sh,v 1.1 2005-05-23 09:28:07 werner-guest Exp $
# 

DUMPDIR=/var/backups
DUMPLDIF=$DUMPDIR/ldap_database.ldif
OLDDUMP=$DUMPDIR/ldap_database.ldif.old

