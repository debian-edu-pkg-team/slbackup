#!/usr/bin/perl -w
#
# slbackup postinst script using debconf
#
# Written by Morten Werner Olsen <werner@skolelinux.no>
#

use strict;
use Debconf::Client::ConfModule ":all";
use Config::General;
use File::Path qw(make_path);
use File::Copy;


## subsections
sub writeconfig {
    my ($datafilename, $data) = @_;
}

## prepare the introduction of some example configurations
my $example_conf = <<EOF;
###---  some example configurations  ---###
#   <localhost>
#       address     localhost
#       location    /etc
#       location    /home
#       location    /var/backups
#       exclude     /home/**/.local/share/Trash/**
#       type        local
#       user        root
#       keep        185
#   </localhost>
#   <externhost>
#       address     extern.domain
#       location    /etc
#       location    /var/backups
#       type        extern
#       user        root
#       keep        50
#   </externhost>
#   <externhost2>
#       address     somehost.domain
#       location    /
#       exclude     /proc
#       exclude     /sys
#       exclude     /tmp
#       exclude_regexp  \.((?i)mpg|avi|mp3|mpeg|wma|wav)\$
#       type        extern
#       user        root
#       keep        30
#   </externhost2>
EOF

## start postinst

if ($ARGV[0] and $ARGV[0] eq "configure") {
    # fetch debconf-variables for slbackup
    my $enable = get("slbackup/enable");
    my $backuptime = get("slbackup/backuptime");
    my $client_name = get("slbackup/client_name");
    my $client_type = get("slbackup/client_type");
    my $client_address = get("slbackup/client_address");
    my $client_user = get("slbackup/client_user");
    my $client_location = get("slbackup/client_location");
    my $client_keep = get("slbackup/client_keep");
    my $server_type = get("slbackup/server_type");
    my $server_destdir = get("slbackup/server_destdir");
    my $server_address = get("slbackup/server_address");
    my $server_user = get("slbackup/server_user");

    # check if config-file (/etc/slbackup/slbackup.conf) exists, and
    # if it does, move it first to (/etc/slbackup/slbackup.conf.dpkg)
    if  ( -e "/etc/slbackup/slbackup.conf" ) {
	move("/etc/slbackup/slbackup.conf","/etc/slbackup/slbackup.conf.dpkg")
    }
	# compose configuration file
	my %confighash;
	my $config = \%confighash;
	$config->{client}->{$client_name}->{address} = $client_address;
	$config->{client}->{$client_name}->{type} = $client_type;
	$config->{client}->{$client_name}->{user} = $client_user;
	my @location = split(/ /, $client_location);
	if (scalar(@location) eq 1) {
	    $config->{client}->{$client_name}->{location} = $location[0];
	} elsif (scalar(@location) gt 1) {
	    @{$config->{client}->{$client_name}->{location}} = @location;
	}
	$config->{client}->{$client_name}->{keep} = $client_keep;
	$config->{server_type} = $server_type;
	$config->{server_destdir} = $server_destdir;
	if ( ! -e "$server_destdir" ) {
		make_path("$server_destdir", {
		                owner => 'root',
		                group => 'root',
		                mode => '0700',
		          }
		);
	}
	$config->{server_address} = $server_address;
	$config->{server_user} = $server_user;

    # write configuration file
    # we no longer do this directly, while since version 2.51
    # of General.pm a configuration that has only one block is written
    # as a named block, while slbackup-php expects a nested block.
    # So the following method has been commented out while we no longer use it.
    # my $datafile = new Config::General();
    # $datafile->save_file("/etc/slbackup/slbackup.conf", $config);

    # Instead we now write the configuration into a variable,
    # reshape within that variable the named block as a nested block
    # and then write the content to file.
    my $datahash = Config::General->new($config);
    my $saved_string = $datahash->save_string();
    $saved_string =~ s{<client }{<client>\n  <};
    $saved_string =~ s{</client>}{  </$client_name>\n$example_conf</client>};
    # now it's ready to be written to file
    my $configfilename = "/etc/slbackup/slbackup.conf";
    open(my $fh, '>', $configfilename) or die "Could not open $configfilename: $!";
    print $fh $saved_string;
    close $fh;

    # check if file specifying cron-job exists and if the user wanted to
    # configure slbackup now, and make one the answers to both questions
    # are "yes"
    if ( ! -e "/etc/cron.d/slbackup" and $enable eq "true") {
	# make cron-job
	my $crontab = "# cron job for Skolelinux Backup (once a day)\n";
	if ($enable eq "false") { $crontab .= "#"; }
	my ($hour, $min) = split(/:/, $backuptime);
	$crontab .= "$min $hour * * * root if [ -x " .
	    "/usr/share/slbackup/slbackup-cron -a -f " .
	    "/etc/slbackup/slbackup.conf ]; then " .
	    "/usr/share/slbackup/slbackup-cron ; fi\n";

	open(CRONFILE, ">/etc/cron.d/slbackup");
	print(CRONFILE "$crontab");
	close(CRONFILE);
    }

}

system('
#DEBHELPER#
');
